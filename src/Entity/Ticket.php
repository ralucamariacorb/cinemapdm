<?php

namespace App\Entity;

use App\Repository\TicketRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TicketRepository::class)
 */
class Ticket
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity=Reservation::class, inversedBy="tickets")
     */
    private $reservation;

    /**
     * @ORM\OneToOne(targetEntity=Seat::class, mappedBy="ticket", cascade={"persist", "remove"})
     */
    private $seat;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getReservation(): ?Reservation
    {
        return $this->reservation;
    }

    public function setReservation(?Reservation $reservation): self
    {
        $this->reservation = $reservation;

        return $this;
    }

    public function getSeat(): ?Seat
    {
        return $this->seat;
    }

    public function setSeat(?Seat $seat): self
    {
        // unset the owning side of the relation if necessary
        if ($seat === null && $this->seat !== null) {
            $this->seat->setTicket(null);
        }

        // set the owning side of the relation if necessary
        if ($seat !== null && $seat->getTicket() !== $this) {
            $seat->setTicket($this);
        }

        $this->seat = $seat;

        return $this;
    }
}
