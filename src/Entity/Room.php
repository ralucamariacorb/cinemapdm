<?php

namespace App\Entity;

use App\Repository\RoomRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RoomRepository::class)
 */
class Room
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $freeSeatsNumber;

    /**
     * @ORM\Column(type="integer")
     */
    private $occupiedSeatsNumber;

    /**
     * @ORM\OneToMany(targetEntity=Seat::class, mappedBy="room", orphanRemoval=true)
     */
    private $seat;

    /**
     * @ORM\OneToMany(targetEntity=Program::class, mappedBy="rooms")
     */
    private $programs;

    public function __construct()
    {
        $this->seat = new ArrayCollection();
        $this->programs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFreeSeatsNumber(): ?int
    {
        return $this->freeSeatsNumber;
    }

    public function setFreeSeatsNumber(int $freeSeatsNumber): self
    {
        $this->freeSeatsNumber = $freeSeatsNumber;

        return $this;
    }

    public function getOccupiedSeatsNumber(): ?int
    {
        return $this->occupiedSeatsNumber;
    }

    public function setOccupiedSeatsNumber(int $occupiedSeatsNumber): self
    {
        $this->occupiedSeatsNumber = $occupiedSeatsNumber;

        return $this;
    }

    /**
     * @return Collection|Seat[]
     */
    public function getSeat(): Collection
    {
        return $this->seat;
    }

    public function addSeat(Seat $seat): self
    {
        if (!$this->seat->contains($seat)) {
            $this->seat[] = $seat;
            $seat->setRoom($this);
        }

        return $this;
    }

    public function removeSeat(Seat $seat): self
    {
        if ($this->seat->removeElement($seat)) {
            // set the owning side to null (unless already changed)
            if ($seat->getRoom() === $this) {
                $seat->setRoom(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Program[]
     */
    public function getPrograms(): Collection
    {
        return $this->programs;
    }

    public function addProgram(Program $program): self
    {
        if (!$this->programs->contains($program)) {
            $this->programs[] = $program;
            $program->setRooms($this);
        }

        return $this;
    }

    public function removeProgram(Program $program): self
    {
        if ($this->programs->removeElement($program)) {
            // set the owning side to null (unless already changed)
            if ($program->getRooms() === $this) {
                $program->setRooms(null);
            }
        }

        return $this;
    }

    public function clearPrograms(): Collection
    {
        $this->programs = new ArrayCollection();

        return $this->programs;
    }
}
