<?php

namespace App\Controller;

use App\Entity\Program;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/program", name="program")
 */
class ProgramController extends AbstractController
{

    public function index(): Response
    {
        return $this->render('program/index.html.twig', [
            'controller_name' => 'ProgramController',
        ]);

        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/HelloWorldController.php',
        ]);
    }

    /**
     * @Route("/get/{movieId}", name="get_all_programs_for_a_movie")
     */
    public function getAllProgramsForAMovie($movieId)
    {
        $encoder = new JsonEncoder();
        // all callback parameters are optional (you can omit the ones you don't use)
        $dateCallback = function ($innerObject, $outerObject, string $attributeName, string $format = null, array $context = []) {
            return $innerObject instanceof \DateTime ? $innerObject->format('Y-m-d') : '';
        };

        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
            AbstractNormalizer::CALLBACKS => [
                'date' => $dateCallback,
            ]
        ];
        $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);

        $serializer = new Serializer([$normalizer], [$encoder]);

        $programs = $this->getDoctrine()->getRepository(Program::class)->findBy([
            'movies' => $movieId
        ]);

        if (!$programs) {
            throw $this->createNotFoundException(
                'No programs! :('
            );
        }

        foreach ($programs as $program){
            $program->clearReservations();
            $program->setMovies(null);
            $program->getRooms()->clearPrograms();
        }

        $jsonContent = $serializer->serialize($programs, 'json');

        return new Response($jsonContent);
    }}
