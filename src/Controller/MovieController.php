<?php

namespace App\Controller;

use App\Entity\Movie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/movie", name="movie")
 */
class MovieController extends AbstractController
{
    /**
     * @Route("/get/{id}", name="get_movie")
     */
    public function getMovieById($id)
    {
        $encoder = new JsonEncoder();
        // all callback parameters are optional (you can omit the ones you don't use)
        $dateCallback = function ($innerObject, $outerObject, string $attributeName, string $format = null, array $context = []) {
            return $innerObject instanceof \DateTime ? $innerObject->format('Y-m-d') : '';
        };

        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
            AbstractNormalizer::CALLBACKS => [
                'premiereDate' => $dateCallback,
                'cinemaDate' => $dateCallback,
                'date' => $dateCallback,
            ]
        ];
        $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);

        $serializer = new Serializer([$normalizer], [$encoder]);

        $movie = $this->getDoctrine()->getRepository(Movie::class)->findOneBy([
            'id' => $id
        ]);

        if (!$movie) {
            throw $this->createNotFoundException(
                'No movie! :('
            );
        }

        $movie->clearPrograms();
        $jsonContent = $serializer->serialize($movie, 'json');

        return new Response($jsonContent);
    }

    /**
     * @Route("/get", name="get_movies")
     */
    public function getMovies(){
        $encoder = new JsonEncoder();
        $dateCallback = function ($innerObject, $outerObject, string $attributeName, string $format = null, array $context = []) {
            return $innerObject instanceof \DateTime ? $innerObject->format('Y-m-d') : '';
        };

        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
            AbstractNormalizer::CALLBACKS => [
                'premiereDate' => $dateCallback,
                'cinemaDate' => $dateCallback,
                'date' => $dateCallback,
            ]
        ];
        $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);
        $serializer = new Serializer([$normalizer], [$encoder]);

        $movies = $this->getDoctrine()->getRepository(Movie::class)->findAll();

        if(!$movies){
            throw $this->createNotFoundException(
                'No movies! :('
            );
        }

        foreach($movies as $movie){
            $movie->clearPrograms();
            $movie->clearGenres();

            $imageData = file_get_contents($movie->getPicture());
            $base64 = base64_encode($imageData);

            $movie->setPicture($base64);
        }

        $jsonContent = $serializer->serialize($movies, 'json');
        return new Response($jsonContent);
    }
}
