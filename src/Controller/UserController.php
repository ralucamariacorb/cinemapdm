<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/user", name="user")
 */
class UserController extends AbstractController
{
    public function index(): Response
    {
        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
        ]);

        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/HelloWorldController.php',
        ]);
    }

    /**
     * @Route("/add/{user}", name="add_user")
     */
    public function add($user)
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        // We have to deserialize the user from the client.User to server.User
        $u = $serializer->deserialize($user, User::class, 'json');
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($u);
        $entityManager->flush();

        // I choosed to resend the serialized server.User back to the client
        // We could send a simple message or anything else
        $jsonContent = $serializer->serialize($u, 'json');

        return new Response($jsonContent);
    }

    /**
     * @Route("/get", name="get_users")
     */
    public function getAllUsers()
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        if (!$users) {
            throw $this->createNotFoundException(
                'No users! :('
            );
        }
        $jsonContent = $serializer->serialize($users, 'json');

        // Uncomment this if you want the see the data from the database in a table

        // return $this->render('user/index.html.twig', [
        //     'users' => $users,
        //     'title' => "Users"
        // ]);

        return new Response($jsonContent);
    }

    /**
     * @Route("/get/{email}/{pass}", name="get_user")
     */
    public function getUserByEmailAndPass($email, $pass)
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'email' => $email,
            'password' => $pass,
        ]);

        if (!$user) {
            throw $this->createNotFoundException(
                'No user! :('
            );
        }

        $jsonContent = $serializer->serialize($user, 'json');

        return new Response($jsonContent);
    }
}
