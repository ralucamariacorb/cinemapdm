<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201228184011 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C849558F93B6FC');
        $this->addSql('DROP INDEX IDX_42C849558F93B6FC ON reservation');
        $this->addSql('ALTER TABLE reservation ADD program_id INT NOT NULL, DROP movie_id, DROP date, DROP hour, DROP minutes');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C849553EB8070A FOREIGN KEY (program_id) REFERENCES program (id)');
        $this->addSql('CREATE INDEX IDX_42C849553EB8070A ON reservation (program_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C849553EB8070A');
        $this->addSql('DROP INDEX IDX_42C849553EB8070A ON reservation');
        $this->addSql('ALTER TABLE reservation ADD date DATE NOT NULL, ADD hour INT NOT NULL, ADD minutes INT NOT NULL, CHANGE program_id movie_id INT NOT NULL');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C849558F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id)');
        $this->addSql('CREATE INDEX IDX_42C849558F93B6FC ON reservation (movie_id)');
    }
}
