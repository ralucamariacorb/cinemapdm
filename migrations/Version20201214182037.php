<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201214182037 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE movie_genre (movie_id INT NOT NULL, genre_id INT NOT NULL, INDEX IDX_FD1229648F93B6FC (movie_id), INDEX IDX_FD1229644296D31F (genre_id), PRIMARY KEY(movie_id, genre_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE movie_genre ADD CONSTRAINT FK_FD1229648F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movie_genre ADD CONSTRAINT FK_FD1229644296D31F FOREIGN KEY (genre_id) REFERENCES genre (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE genre_movie');
        $this->addSql('ALTER TABLE genre CHANGE name name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE movie DROP FOREIGN KEY FK_1D5EF26F3EB8070A');
        $this->addSql('DROP INDEX IDX_1D5EF26F3EB8070A ON movie');
        $this->addSql('ALTER TABLE movie DROP program_id, CHANGE rating rating VARCHAR(255) NOT NULL, CHANGE type type VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE program ADD movies_id INT DEFAULT NULL, ADD rooms_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE program ADD CONSTRAINT FK_92ED778453F590A4 FOREIGN KEY (movies_id) REFERENCES movie (id)');
        $this->addSql('ALTER TABLE program ADD CONSTRAINT FK_92ED77848E2368AB FOREIGN KEY (rooms_id) REFERENCES room (id)');
        $this->addSql('CREATE INDEX IDX_92ED778453F590A4 ON program (movies_id)');
        $this->addSql('CREATE INDEX IDX_92ED77848E2368AB ON program (rooms_id)');
        $this->addSql('ALTER TABLE room DROP FOREIGN KEY FK_729F519B3EB8070A');
        $this->addSql('DROP INDEX IDX_729F519B3EB8070A ON room');
        $this->addSql('ALTER TABLE room DROP program_id');
        $this->addSql('ALTER TABLE seat ADD col INT NOT NULL, CHANGE row row VARCHAR(255) NOT NULL, CHANGE seat_column room_id INT NOT NULL');
        $this->addSql('ALTER TABLE seat ADD CONSTRAINT FK_3D5C366654177093 FOREIGN KEY (room_id) REFERENCES room (id)');
        $this->addSql('CREATE INDEX IDX_3D5C366654177093 ON seat (room_id)');
        $this->addSql('ALTER TABLE user ADD phone VARCHAR(255) NOT NULL, DROP phone_number, CHANGE firstname firstname VARCHAR(255) NOT NULL, CHANGE lastname lastname VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE genre_movie (genre_id INT NOT NULL, movie_id INT NOT NULL, INDEX IDX_A058EDAA4296D31F (genre_id), INDEX IDX_A058EDAA8F93B6FC (movie_id), PRIMARY KEY(genre_id, movie_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE genre_movie ADD CONSTRAINT FK_A058EDAA4296D31F FOREIGN KEY (genre_id) REFERENCES genre (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE genre_movie ADD CONSTRAINT FK_A058EDAA8F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE movie_genre');
        $this->addSql('ALTER TABLE genre CHANGE name name VARCHAR(50) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE movie ADD program_id INT DEFAULT NULL, CHANGE rating rating VARCHAR(50) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE type type VARCHAR(20) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE movie ADD CONSTRAINT FK_1D5EF26F3EB8070A FOREIGN KEY (program_id) REFERENCES program (id)');
        $this->addSql('CREATE INDEX IDX_1D5EF26F3EB8070A ON movie (program_id)');
        $this->addSql('ALTER TABLE program DROP FOREIGN KEY FK_92ED778453F590A4');
        $this->addSql('ALTER TABLE program DROP FOREIGN KEY FK_92ED77848E2368AB');
        $this->addSql('DROP INDEX IDX_92ED778453F590A4 ON program');
        $this->addSql('DROP INDEX IDX_92ED77848E2368AB ON program');
        $this->addSql('ALTER TABLE program DROP movies_id, DROP rooms_id');
        $this->addSql('ALTER TABLE room ADD program_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE room ADD CONSTRAINT FK_729F519B3EB8070A FOREIGN KEY (program_id) REFERENCES program (id)');
        $this->addSql('CREATE INDEX IDX_729F519B3EB8070A ON room (program_id)');
        $this->addSql('ALTER TABLE seat DROP FOREIGN KEY FK_3D5C366654177093');
        $this->addSql('DROP INDEX IDX_3D5C366654177093 ON seat');
        $this->addSql('ALTER TABLE seat ADD seat_column INT NOT NULL, DROP room_id, DROP col, CHANGE row row VARCHAR(1) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE user ADD phone_number VARCHAR(10) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, DROP phone, CHANGE firstname firstname VARCHAR(50) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE lastname lastname VARCHAR(50) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
